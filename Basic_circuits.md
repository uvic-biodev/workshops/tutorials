Fundamental Equations
---
Ohm's Law: $`V = IR`$

KCL: At any node in a circuit $`\displaystyle \sum_{k} i_k = 0`$

KVL: In any loop within a circuit $`\displaystyle \sum_{k} v_k = 0`$

Voltage divider:
---
```math
V_{o} = \frac{R_2}{R_1 + R_2} V_{cc}
```

Highpass Circuit:
---
```math
v_o = \frac{j\omega RC}{1 + j\omega RC} v_s
```
```math
as\ \omega \rightarrow 0, v_o \rightarrow 0
```
```math
as\ \omega \rightarrow \infty, v_o \rightarrow v_s
```
